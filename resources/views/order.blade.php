@extends('main')

@section('content')

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>Add a new order</h3>
            <form method="post" action="/addOrder">
                <hr/>
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="customerId">Customer</label>
                    <select id="customerId" name="customerId" class="form-control">
                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">
                                {{$customer->customer_name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="shipperId">Shipper</label>
                    <select id="shipperId" name="shipperId" class="form-control">
                        @foreach($shippers as $shipper)
                            <option value="{{$shipper->id}}">
                                {{$shipper->shipper_name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label for="categoryId">Category</label>
                            <select id="categoryId" name="categoryId" class="form-control">
                                <option value="all">All</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">
                                        {{$category->category_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label for="categoryId">Supplier</label>
                            <select id="supplierId" name="supplierId" class="form-control">
                                <option value="all">All</option>
                                @foreach($suppliers as $supplier)
                                    <option value="{{$supplier->id}}">
                                        {{$supplier->supplier_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label for="productId">Product</label>

                            <select id="productId" name="productId" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <input type="number" id="quantity" class="form-control" name="quantity"/>
                </div>
                <div class="form-group" id="priceDiv" hidden>
                    <label for="price">Price</label>
                    <input type="number" class="form-control" name="price" id="price" readonly/>
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <br/>
           @include('errors')
        </div>
    </div>


@endsection