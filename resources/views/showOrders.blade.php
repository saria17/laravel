@extends('main')
@section('content')

    <div class="row">
        <hr/>
        <div class="col-lg-9 col-lg-offset-2">
            <input type="hidden" value="{{$orderCount}}" id="orderCount"/>
            <div class="row">
                <div class="col-sm-8">
                    <button id="advancedSearch" class="btn btn-primary">Advanced search</button>
                </div>
            </div>
            <br/>
            <div class="row" id="advancedSearchForm" hidden>
                <div class="col-md-12">

                    <div class="row">

                        <div class="col-md-4">
                            <label for="category">Category </label>
                            <div class="form-group">

                                <select id="categoryId" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">
                                            {{$category->category_name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="supplier"> Supplier</label>
                            <div class="form-group">

                                <select id="supplierId" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">
                                            {{$supplier->supplier_name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="product">Product </label>
                            <div class="form-group">

                                <select id="productId" class="form-control">
                                </select>
                            </div>

                        </div>

                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="customer">Customer </label>
                            <br/>

                            <select id="customer" class="form-control">
                                <option value="">All</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}">
                                        {{$customer->customer_name}}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                        <div class="col-md-6">
                            <label for="shipper">Shipper</label>
                            <br/>
                            <select id="shipper" class="form-control">
                                <option value="">All</option>
                                @foreach($shippers as $shipper)
                                    <option value="{{$shipper->id}}">
                                        {{$shipper->shipper_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="orderDate">Order date </label>
                            <div class="form-group">
                                From: <input class="form-control" type="date" name="orderDateFrom"
                                             id="orderDateFrom">
                            </div>
                            <div class="form-group">
                                To: <input class="form-control" type="date" name="orderDateTo" id="orderDateTo">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="orderPrice">Order Price </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        From: <input class="form-control" type="number" name="orderPriceFrom"
                                                     id="orderPriceFrom">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        To: <input class="form-control" type="number" name="orderPriceTo"
                                                   id="orderPriceTo">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="orderQuantity">Quantity </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        From: <input class="form-control" type="number" name="orderQuantityFrom"
                                                     id="orderQuantityFrom">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        To: <input class="form-control" type="number" name="orderQuantityTo"
                                                   id="orderQuantityTo">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <button class="btn btn-default" id="search">Search</button>
                        </div>

                    </div>

                </div>
            </div>
            @include('errors')
            <hr/>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Choose page size</label>
                    <select class="form-control" id="pageSize">
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                    </select>

                </div>
                <div class="col-sm-4">
                    <label for="">Order by</label>
                    <select class="form-control" id="orderBy">
                        <option value="orderId">Order id</option>
                        <option value="customerName">Customer</option>
                        <option value="employeeName">Employee</option>
                        <option value="orderDate">Order Date</option>
                        <option value="shipperName">Shipper</option>
                        <option value="productName">Product</option>
                        <option value="quantity">Quantity</option>
                        <option value="price">Price</option>
                    </select>

                </div>
                <div class="col-sm-2">
                    <label for="">Sort</label>
                    <select class="form-control" id="sort">
                        <option value="asc">Increasing</option>
                        <option value="desc">Decreasing</option>

                    </select>
                </div>

            </div>

            <hr/>

            <table id="orders" class="table">
                <thead>
                <tr>
                    <th>Order id</th>
                    <th>Customer</th>
                    <th>Employee</th>
                    <th>Order Date</th>
                    <th>Shipper</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>

                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>
            <div class="row">
                <div class="col-md-12">
                    <div id="pages" class="col-sm-8">

                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection