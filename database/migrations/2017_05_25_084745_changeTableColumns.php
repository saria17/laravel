<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('customers', function (Blueprint $table) {

            $table->renameColumn('"contactName"', 'contact_name');
            $table->renameColumn('"postalCode"', 'postal_code');
        });
        Schema::table('employees', function (Blueprint $table) {

            $table->renameColumn('"firstName"', 'first_name');
            $table->renameColumn('"lastName"', 'last_name');
            $table->renameColumn('"birthDate"', 'birth_date');
        });

        Schema::table('categories', function (Blueprint $table) {

            $table->renameColumn('"categoryName"', 'category_name');
        });
        Schema::table('shippers', function (Blueprint $table) {

            $table->renameColumn('"shipperName"', 'shipper_name');
        });
        Schema::table('suppliers', function (Blueprint $table) {

            $table->renameColumn('"supplierName"', 'supplier_name');
            $table->renameColumn('"contactName"', 'contact_name');
            $table->renameColumn('"postalCode"', 'postal_code');
        });
        Schema::table('products', function (Blueprint $table) {

            $table->renameColumn('"productName"', 'product_name');
        });






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
