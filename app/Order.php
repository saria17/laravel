<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function employee()
    {
        return $this->belongsTo('App\Employee');

    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function shipper()
    {
        return $this->belongsTo('App\Shipper');
    }

    public function orderDetail(){
        return $this->hasOne('App\OrderDetail');
    }


}
