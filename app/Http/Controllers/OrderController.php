<?php

namespace App\Http\Controllers;

use DB;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Exception;

class OrderController extends Controller
{
    //
    public function addOrder()
    {
        $customers = \App\Customer::all();
        $shippers = \App\Shipper::all();
        $categories = \App\Category::all();
        $suppliers = \App\Supplier::all();
        return view('order', ['customers' => $customers, 'shippers' => $shippers,
            'categories' => $categories, 'suppliers' => $suppliers]);
    }

    public function addNewOrder(Request $request)
    {
        $customerId = $request->customerId;
        $shipperId = $request->shipperId;
        $productId = $request->productId;
        $employee = \App\Employee::find(1);
        $quantity = $request->quantity;

        $this->validate($request,[
            'customerId'=>'required|exists:customers,id',
            'shipperId'=>'required|exists:shippers,id',
            'productId'=>'required|exists:products,id',
            'quantity'=>'required',

            ]);

        $order = new \App\Order;
        $orderDetail = new \App\OrderDetail;

        $order->customer_id = $customerId;
        $order->shipper_id = $shipperId;
        $order->employee_id = $employee->id;
        $order->save();

        $orderDetail->order_id = $order->id;
        $orderDetail->product_id = $productId;
        $orderDetail->quantity = $quantity;

        $orderDetail->save();

        return redirect('/showOrders');
    }

    public function show()
    {
        $count = \App\Order::count();
        $categories = \App\Category::all();
        $shippers = \App\Shipper::all();
        $suppliers = \App\Supplier::all();
        $customers = \App\Customer::all();

        return view('showOrders', ['orderCount' => $count, 'categories' => $categories, 'shippers' => $shippers,
            'customers' => $customers, 'suppliers' => $suppliers]);
    }

    public function getOrders(Request $request)
    {

        $pageSize = $request->pageSize;
        $pageNum = $request->pageNum;
        $categoryID = $request->categoryID;
        $supplierID = $request->supplierID;
        $productID = $request->productID;
        $customerID = $request->customerID;
        $shipperID = $request->shipperID;
        $orderPriceFrom = $request->orderPriceFrom;
        $orderPriceTo = $request->orderPriceTo;
        $orderDateFrom = $request->orderDateFrom;
        $orderDateTo = $request->orderDateTo;
        $orderQuantityFrom = $request->orderQuantityFrom;
        $orderQuantityTo = $request->orderQuantityTo;
        $orderBy = $request->orderBy;
        $sort = $request->sort;
        $query = null;
        $orderQuery = null;

        try {

            if (is_numeric($categoryID)) {
                $query = " and p.category_id='$categoryID'";
            }
            if (is_numeric($supplierID)) {
                $query .= " and p.supplier_id='$supplierID'";
            }
            if (is_numeric($productID)) {
                $query .= " and p.id='$productID'";
            }
            if (is_numeric($customerID)) {
                $query .= " and o.customer_id='$customerID'";
            }
            if (is_numeric($shipperID)) {
                $query .= " and o.shipper_id='$shipperID'";
            }
            if (is_numeric($orderPriceFrom) && is_numeric($orderPriceTo) &&
                $orderPriceFrom > 0 && ($orderPriceTo > 0 &&
                    $orderPriceTo >= $orderPriceFrom)
            ) {
                $query .= " and (p.price*od.quantity>='$orderPriceFrom' and p.price*od.quantity<='$orderPriceTo')";
            }
            if (is_numeric($orderQuantityFrom) && is_numeric($orderQuantityTo) &&
                $orderQuantityFrom > 0 && ($orderQuantityTo > 0 &&
                    $orderQuantityTo >= $orderQuantityFrom)
            ) {
                $query .= " and (od.quantity>='$orderQuantityFrom' and od.quantity<='$orderQuantityTo')";
            }
            if (!is_null($orderDateFrom) && !is_null($orderDateTo)) {
                $query .= " and (o.created_at>='$orderDateFrom' and o.created_at<='$orderDateTo')";
            }
            if (!is_null($orderBy) && $orderBy !== '') {
                switch ($orderBy) {
                    case 'orderId':
                        $orderQuery = ' o.id';
                        break;
                    case 'customerName':
                        $orderQuery = ' c.customer_name';
                        break;
                    case 'employeeName':
                        $orderQuery = ' e.first_name';
                        break;
                    case 'orderDate':
                        $orderQuery = ' o.created_at';
                        break;
                    case 'shipperName':
                        $orderQuery = ' s.shipper_name';
                        break;
                    case 'productName':
                        $orderQuery = ' p.product_name';
                        break;
                    case 'quantity':
                        $orderQuery = ' od.quantity';
                        break;
                    case 'price':
                        $orderQuery = ' p.price*od.quantity';
                        break;
                }
            }
            if (!is_null($sort) && $sort !== '') {
                switch ($sort) {
                    case 'asc':
                        $orderQuery .= " asc";
                        break;
                    case 'desc':
                        $orderQuery .= " desc";
                        break;
                }
            }
            $orders = DB::select('select json_agg(data.*) from getOrders(?,?,?,?) as data', array($pageSize, $pageNum, $query, $orderQuery));
            return $orders;
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }
}
