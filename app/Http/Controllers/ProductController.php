<?php

namespace App\Http\Controllers;

use App\Product;
use App\Supplier;
use Illuminate\Http\Request;
use \App\Category;

class ProductController extends Controller
{
    //
    public function getProduct(\App\Product $product)
    {
        return $product;
    }

    public function getProducts($categoryId, $supplierId)
    {
        $products = null;

        if (is_numeric($categoryId) && is_numeric($supplierId)) {
           // echo 'category int supplier int';
            $products = Category::find($categoryId)->products->where('supplier_id', $supplierId);
        } elseif (is_numeric($categoryId) && !is_numeric($supplierId)) {
           // echo 'category int supplier not int';
            $products = Category::find($categoryId)->products;
        } elseif (is_numeric($supplierId) && !is_numeric($categoryId)) {
          //  echo 'category not int supplier int';
            $products = Supplier::find($supplierId)->products;
        } else {
          //  echo 'neither';
            $products = Product::all();
        }

        return $products;
    }

}
