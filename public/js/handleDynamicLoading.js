/**
 * Created by saria on 5/23/2017.
 */
$(document).ready(function () {
    getPages();
    getOrders(1);

    $('select').select2();
    let categoryId = $('#categoryId').val();
    let supplierId = $('#supplierId').val();
    getData(categoryId, supplierId, getPrice);
    $('#categoryId').on('change', function () {
        let categoryId = $('#categoryId').val();
        let supplierId = $('#supplierId').val();
        getData(categoryId, supplierId, getPrice);

    });
    $('#supplierId').on('change', function () {
        categoryId = $('#categoryId').val();
        supplierId = $('#supplierId').val();
        getData(categoryId, supplierId, getPrice);

    });
    $('#quantity').on('change', function () {
        getPrice();
    });
    $('#productId').on('change', function () {
        getPrice();
    });
    $('#quantity').on('keydown', function () {
        getPrice();
    });
    $('body').on('click', '.pageNum', function (data) {
        getOrders(data.target.value);
    });
    $('#pageSize').on('change', function () {
        getOrders(1);
    });
    $('body').on('click', '#advancedSearch', function (event) {
        $('#advancedSearchForm').attr('hidden', false);

        event.target.textContent = "Hide ";
        event.target.className = "btn btn-danger";
        event.target.id = "hideAdvancedSearch";

    });
    $('body').on('click', '#hideAdvancedSearch', function (event) {
        $('#advancedSearchForm').attr('hidden', true);
        event.target.textContent = "Advanced search";
        event.target.className = "btn btn-primary";
        event.target.id = "advancedSearch";

    });
    $('body').on('click', '#search', function (event) {
        getOrders(1);
    });
    $('#orderBy').on('change', function (event) {
    getOrders(1);
    });
    $('#sort').on('change', function (event) {
        getOrders(1);
    });
});
function getData(categoryId, supplierId, callback) {
    let option = $('#productId');
    option.empty();
    option.append($('<option/>').val('').text('All'));

    $.get('/getProducts/' + categoryId + '/' + supplierId, function (data) {
        data.forEach(function (product) {

            option.append($('<option/>').val(product.id).text(product.product_name));
        });
        callback();
    });

}
let getPrice = function () {
    let productId = $('#productId').val();
    if (productId) {
        $.get('/getProduct/' + productId, function (data) {
            let quantity = $('#quantity').val();
            setPrice(quantity * data.price);
        });
    } else {
        setPrice(0);
    }

};
function setPrice(value) {
    if (value && value > 0) {
        $('#priceDiv').attr('hidden', false);
        $('#price').val(value)
    } else {
        $('#priceDiv').attr('hidden', true);
        $('#price').empty();
    }
}
let getOrders = function (pageNum) {
    getPages();
    let table = $('#orders tbody');
    let pageSize = $('#pageSize').val();
    let orderBy=$('#orderBy').val();
    let categoryID = $('#categoryId').val();
    let supplierID = $('#supplierId').val();
    let productID = $('#productId').val();
    let customerID = $('#customer').val();
    let shipperID = $('#shipper').val();
    let orderPriceFrom = $('#orderPriceFrom').val();
    let orderPriceTo = $('#orderPriceTo').val();
    let orderDateFrom = $('#orderDateFrom').val();
    let orderDateTo = $('#orderDateTo').val();
    let orderQuantityFrom = $('#orderQuantityFrom').val();
    let orderQuantityTo = $('#orderQuantityTo').val();
    let sort=$('#sort').val();
    // console.log("orderDateFrom "+ orderDateFrom);
    // console.log("orderDateTo "+orderDateTo);
    if (table && pageSize) {
        table.empty();
        $.post('/getOrders',
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                "pageNum": pageNum,
                "pageSize": pageSize,
                "categoryID": categoryID,
                "supplierID": supplierID,
                "productID": productID,
                "customerID": customerID,
                "shipperID": shipperID,
                "orderDateFrom": orderDateFrom,
                "orderDateTo": orderDateTo,
                "orderPriceFrom": orderPriceFrom,
                "orderPriceTo": orderPriceTo,
                "orderQuantityFrom": orderQuantityFrom,
                "orderQuantityTo": orderQuantityTo,
                "orderBy":orderBy,
                "sort":sort
            }).done(
            function (data) {
                 //console.log(data);
                data.forEach(function (order) {
                    if (order.json_agg) {
                        let ordersArray = JSON.parse(order.json_agg);
                        ordersArray.forEach(function (o) {
                            table.append('<tr><td>' + o.outcustomerid + '</td>' +
                                '<td>' + o.outcustomername + '</td>'
                                + '<td>' + o.outemployyefirstname + '</td>'
                                + '<td>' + o.outordercreatedat + '</td>'
                                + '<td>' + o.outshippername + '</td>'
                                + '<td>' + o.outproductname + '</td>'
                                + '<td>' + o.outquantity + '</td>'
                                + '<td>' + o.outprice * o.outquantity + '</td> </tr>')
                        })

                    }

                });

            });

    }

};
let getPages = function () {
    let pageDiv = $('#pages');
    pageDiv.empty();
    let count = $('#orderCount').val();
    let pageSize = $('#pageSize').val();
    let pageCount = count / pageSize;

    if (isDecimal(pageCount)) {
        pageCount = pageCount + 1;
    }
    for (let i = 1; i <= pageCount; i++) {
        let button = document.createElement('button');
        button.className = "pageNum btn btn-default";
        button.value = i;
        button.textContent = i;
        pageDiv.append(button);
    }
};
let isDecimal = function (value) {
    return value !== Math.floor(value);
};
