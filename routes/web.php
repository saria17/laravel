<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/addOrder', 'OrderController@addOrder');
Route::post('/addOrder', 'OrderController@addNewOrder');
Route::get('/getProducts/{categoryId}/{supplierId}', 'ProductController@getProducts');
Route::get('/getProduct/{product}', 'ProductController@getProduct');
Route::post('/getOrders', 'OrderController@getOrders');
Route::get('/showOrders', 'OrderController@show');